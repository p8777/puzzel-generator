CREATE TABLE IF NOT EXISTS users
(
    id INT NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE IF NOT EXISTS puzzels
(
    Pid INT AUTO_INCREMENT,
    nRows INT NOT NULL,
    nColoms INT NOT NULL,
    pSave INT NOT NULL,
    hint INT,
    title varchar(20),
    img varchar(15),
    reward varchar(20),
    PRIMARY KEY (Pid)
);
CREATE TABLE IF NOT EXISTS users_puzzels
(
    id INT NOT NULL REFERENCES users(id),
    Pid INT NOT NULL REFERENCES puzzels(Pid),
    puzzelState JSON,
    PRIMARY KEY (id,Pid)
);