<!DOCTYPE html>
//todo hidden messages voor input errors weergeven
<html>
	<head>
		<title> Puzzel - WAI-NOT</title>
		<link rel='stylesheet' href='stylesheet.css'>
	</head>
	<body>
		<h1>Puzzel - WAI-NOT</h1>
		<form action="process_form.php" method="post">
			<label for='titel' class='titles' id='titel'> Titel:</label><br>
			<input type='text' id='titel_input' name='titel' value='<?php echo $name;?>'><br>
			<span class="error">*<?php echo $titelErr;?></span>
			<label for='afbeelding' class='titles' id='afbeelding'> Afbeelding:</label><br>
			<input type='file' id='afbeelding_input' name='afbeelding' accept='.png,.jpeg,.jpg,.pdf'><br>
			<span class="error">*<?php echo $afbeeldingErr;?></span>
			<label for='hor' class='titles' id='hor'> Horizontale stukken:</label><br>
			<input type='text' id='hor_input' name='hor' value='<?php echo $hor_input;?>'><br>
			<span class="error">*<?php echo $horErr;?></span>
			<label for='ver' class='titles' id='ver'> Verticale stukken:</label><br>
			<input type='text' id='ver_input' name='ver' value='<?php echo $ver;?>'><br>
			<span class="error">*<?php echo $verErr;?></span>
			<label for='beloning' class='titles' id='beloning'> Beloningswoord:</label><br>
			<input type='text' id='beloning_input' name='beloning' value='<?php echo $beloning;?>'><br>
			<label for='geluid' class='titles' id='geluid'> Geluidsfragment beloning:</label><br>
			<input type='file' id='geluid_input' name='geluid' accept='.mp3,.mp4,.wav,.ogg,.aac'><br>
			<div>
				<label for='toon_hint' class='titles'>Toon hint:</label>
					<a href='#' class='info'> <img src='info.png' alt='info?' src='info.png' id='info_afbeelding'><br>
						<span> Sta leerlingen toe om een voorvertoning van de voltooide puzzel te zien.</span>
					</a>
				<input type='checkbox' id='hint_input' name='hint' <?php if (isset($hint)) echo "checked";?>>Toon hint<br>
			</div>
			<fieldset>
				<div>
					<label for='instructies_head' class='titles' id='instuctions'>Instructies:</label>
					<a href='#' class='info'> <img src='info.png' alt='info?' src='info.png' id='info_afbeelding'><br>
						<span> Instructies voor de leerling. Ze worden weergegeven wanneer de widget wordt gestart en kunnen met m.b.v. de info-knop worden weergegeven/verborgen.</span>
					</a><br>
				</div>
				<div>
					<label for='instr_titel' class='titles'>Titel:</label><br>
					<input type='text' id='instr_titel_input' name='instr_titel' value='Instructies'><br>
					<label for='instructies' class='titles'>Instructies:</label><br>
					<textarea name='instructies' rows='5' cols='50' value='<?php echo $instructies;?>'></textarea>
				</div>
			</fieldset>
			<fieldset>
				<div>
					<label for='opslaan_optie' class='titles' id='opslaan_optie'> Antwoorden opslaan en laden?</label>
					<a href='#' class='info'> <img src='info.png' alt='info?' src='info.png' id='info_afbeelding'><br>
						<span> Indien 'ja' kunnen studenten hun voortgang van hun puzzel opslaan en later aan verder werken</span>
					</a><br>
				</div>
				<label for='opslaan'> Ja</label>
				<input type='radio' id='opslaan_ja_input' name='opslaan'  <?php if (isset($opslaan) && $opslaan=="Ja") echo "checked";?>><br>
				<label for='opslaan' > Nee</label>
				<input type='radio' id='opslaan_nee_input' name='opslaan' <?php if (isset($opslaan) && $opslaan=="Nee") echo "checked";?>><br>
			</fieldset>
			<input type="reset" value="Reset">
			<input type="submit" value="Klaar">
		<form>
	</body>
</html>
<?php

$titel = $afbeelding = $hor = $ver = $beloning = $geluid = $hint = $instr_titel = $instructies = $opslaan ="";
$titelErr = $afbeeldingErr = $horErr = $verErr = $beloningErr = $geluidErr = $hintErr = $instr_titelErr = $instructiesErr = $opslaanErr ="";



if ($_SERVER["REQUEST_METHOD"]=="POST"){
  $titel = test_input($_POST[""]);
  $hor = test_input($_POST[""]);
  $ver = test_input($_POST[""]);
  $beloning = test_input($_POST[""]);
  $instr_titel = test_input($_POST[""]);
  $instructies = test_input($_POST[""]);

  if (empty($_POST["titel"])){
    $titelErr = "Veld is verplicht";
  } else{
    $titel = test_input($_POST["titel"]);
  }

  if (empty($_POST["afbeelding"])){
    $afbeeldingErr = "Veld is verplicht";
  } else{
    $afbeelding = test_input($_POST["afbeelding"]);
  }

  if (empty($_POST["hor"])){
    $horErr = "Veld is verplicht";
  } else{
    $hor = test_input($_POST["hor"]);
  }

  if (empty($_POST["ver"])){
    $verErr = "Veld is verplicht";
  } else{
    $ver = test_input($_POST["ver"]);
  }

}


//server safety measurement for textarea
function test_input($data){
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
