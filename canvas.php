<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- <script src="puzzel.js" defer type="module"></script> -->
    <script
      type="text/javascript"
      src="http://code.jquery.com/jquery.min.js"
    ></script>
    <script src="js/applicatie.js" defer type="module">
    </script>


    <title>Document</title>
  </head>
  <body>
    <button id="ontordenbutton" >Ontorden</button>
    <canvas id="canvas" style="border: 1px solid red"></canvas>
  </body>
  
</html>
