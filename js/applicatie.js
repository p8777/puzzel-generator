import Puzzel from "./puzzel.js";
import Vector from "./vector.js";

var canvas = document.getElementById("canvas");
var src =
  "https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832_960_720.jpg";
// "https://cdn.pixabay.com/photo/2022/04/03/15/32/flower-7109258_960_720.jpg";

canvas.width = 1500;
canvas.height = 700;

var $canvas = $("#canvas");
var canvasOffset = $canvas.offset();
var offset = new Vector(canvasOffset.left, canvasOffset.top);
var scroll = new Vector(0, 0);

var mouse = new Vector();
var start = new Vector();
var selectedPiece = new Vector();
var puzzel;

// image deel
var img = new Image();
img.onload = function () {
  var imgDim = new Vector(img.width, img.height);
  console.log(imgDim);
  puzzel = new Puzzel(imgDim, 5, 4, canvas, img);
};
img.src = src;

// mouse deel
function handleMouseDown(e) {
  e.preventDefault();

  start.update(parseInt(e.clientX), parseInt(e.clientY));
  scroll.update(Math.round(window.scrollX), Math.round(window.scrollY));
  start.minus(offset);
  start.add(scroll);

  console.log("startx: " + start.x + ", scrollx: " + scroll.x);
  console.log("starty: " + start.y + ", scrolly: " + scroll.y);
  selectedPiece = puzzel.clickedPiece(start);
  if (selectedPiece.x != null) {
    console.log("hit!");
  }
}

// done dragging
function handleMouseUp(e) {
  if (selectedPiece.x == null) {
    return;
  }
  e.preventDefault();
  puzzel.connection(selectedPiece.x, selectedPiece.y);
  selectedPiece.null();
}
// also done dragging
function handleMouseOut(e) {
  e.preventDefault();
  selectedPiece.null();
}
function handleMouseMove(e) {
  if (selectedPiece.x == null) {
    return;
  }
  e.preventDefault();
  mouse.update(parseInt(e.clientX), parseInt(e.clientY));
  scroll.update(Math.round(window.scrollX), Math.round(window.scrollY));
  mouse.minus(offset);
  mouse.add(scroll);

  var diff = mouse.diff(start);
  start = mouse.copy();

  puzzel.movePiece(diff, selectedPiece.x, selectedPiece.y);
}

$("#ontordenbutton").on("click", function () {
  puzzel.scrabble();
});

// listen for mouse events
$("#canvas").mousedown(function (e) {
  handleMouseDown(e);
});
$("#canvas").mousemove(function (e) {
  handleMouseMove(e);
});
$("#canvas").mouseup(function (e) {
  handleMouseUp(e);
});
$("#canvas").mouseout(function (e) {
  handleMouseOut(e);
});
