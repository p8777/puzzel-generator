import Vector from "./vector.js";

export default class Piece {
  shape;
  size;
  cord;
  row;
  col;
  img;
  shape;
  connection;

  constructor(cord, col, row, img, shape, conn, size) {
    this.shape = shape;
    this.cord = cord;
    this.row = row;
    this.col = col;
    this.img = img;
    this.size = size;
    this.connection = conn;
  }
  draw(context) {
    if (false) {
      context.beginPath();
      context.moveTo(this.cord.x, this.cord.y);
      context.strokeStyle = "blue";
      context.lineTo(this.cord.x + this.size.x, this.cord.y);
      context.lineTo(this.cord.x + this.size.x, this.cord.y + this.size / 4);
      context.arc(
        this.cord.x + this.size.x,
        this.cord.y + this.size.y / 2,
        this.size.y / 4,
        -Math.PI / 2,
        Math.PI / 2
      );
      context.lineTo(this.cord.x + this.size.x, this.cord.y + this.size.y);
      context.lineTo(this.cord.x, this.cord.y + this.size.y);
      context.arc(
        this.cord.x,
        this.cord.y + this.size.y / 2,
        this.size.y / 4,
        Math.PI / 2,
        -Math.PI / 2,
        true
      );
      context.lineTo(this.cord.x, this.cord.y);
      context.stroke();
      context.clip();
    } else {
      context.beginPath();
      context.strokeStyle = "black";
      context.moveTo(this.cord.x, this.cord.y);
      this.drawTop(this.shape.top, context);
      this.drawRight(this.shape.right, context);
      this.drawBottom(this.shape.bottom, context);
      this.drawLeft(this.shape.left, context);
      context.stroke();
      context.clip();
      if (this.row === 0 && this.col === 0) {
        context.drawImage(this.img, this.cord.x, this.cord.y);
      } else if (this.row === 0) {
        context.drawImage(
          this.img,
          0,
          this.col * this.size.y - this.size.x / 4,
          1000,
          1000,
          this.cord.x,
          this.cord.y - this.size.x / 4,
          1000,
          1000
        );
      } else if (this.col === 0) {
        context.drawImage(
          this.img,
          this.row * this.size.x - this.size.y / 4,
          0,
          1000,
          1000,
          this.cord.x - this.size.y / 4,
          this.cord.y,
          1000,
          1000
        );
      } else {
        context.drawImage(
          this.img,
          this.row * this.size.x - this.size.y / 4,
          this.col * this.size.y - this.size.x / 4,
          1000,
          1000,
          this.cord.x - this.size.y / 4,
          this.cord.y - this.size.x / 4,
          1000,
          1000
        );
      }
    }
  }
  drawTop(side, context) {
    if (side == 0) {
      context.lineTo(this.cord.x + this.size.x, this.cord.y);
    } else {
      var v = this.cord;
      var s = this.size;
      var rev = side == 1 ? false : true;
      context.lineTo(v.x + s.x / 4, v.y);
      context.arc(v.x + s.x / 2, v.y, s.x / 4, Math.PI, 0, rev);
      context.lineTo(v.x + s.x, v.y);
    }
  }
  drawBottom(side, context) {
    if (side == 0) {
      context.lineTo(this.cord.x, this.cord.y + this.size.y);
    } else {
      var rev = side == 1 ? false : true;
      var v = this.cord.newAdd(this.size);
      var s = this.size;
      var rev = side == 1 ? false : true;
      context.lineTo(v.x - s.x / 4, v.y);
      context.arc(v.x - s.x / 2, v.y, s.x / 4, 0, Math.PI, rev);
      context.lineTo(v.x - s.x, v.y);
    }
  }
  drawLeft(side, context) {
    if (side == 0) {
      context.lineTo(this.cord.x, this.cord.y);
    } else {
      var v = this.cord.newAddY(this.size.y);
      var s = this.size;
      var rev = side == 1 ? false : true;
      context.lineTo(v.x, v.y - s.y / 4);
      context.arc(v.x, v.y - s.y / 2, s.y / 4, Math.PI / 2, -Math.PI / 2, rev);
      context.lineTo(v.x, v.y - s.y);
    }
  }
  drawRight(side, context) {
    if (side == 0) {
      context.lineTo(this.cord.x + this.size.x, this.cord.y + this.size.y);
    } else {
      var v = this.cord.newAddX(this.size.x);
      var s = this.size;
      var rev = side == 1 ? false : true;
      context.lineTo(v.x, v.y + s.y / 4);
      context.arc(v.x, v.y + s.y / 2, s.y / 4, -Math.PI / 2, Math.PI / 2, rev);
      context.lineTo(v.x, v.y + s.y);
    }
  }
  hitPiece(v) {
    return (
      v.x >= this.cord.x &&
      v.x <= this.cord.x + this.size.x &&
      v.y >= this.cord.y &&
      v.y <= this.cord.y + this.size.y
    );
  }
  move(v) {
    this.cord.add(v);
  }
  moveb(v) {
    this.cord.minus(v);
  }
  connect(p, dir) {
    if (dir === 0 && this.cord.close(p.cord.newAddY(p.size.y))) {
      return this.cord.diff(p.cord.newAddY(p.size.y));
    } else if (dir === 1 && this.cord.newAddY(this.size.y).close(p.cord)) {
      return this.cord.newAddY(this.size.y).diff(p.cord);
    } else if (dir === 2 && this.cord.close(p.cord.newAddX(p.size.x))) {
      return this.cord.diff(p.cord.newAddX(p.size.x));
    } else if (dir === 3 && this.cord.newAddX(this.size.x).close(p.cord)) {
      return this.cord.newAddX(this.size.x).diff(p.cord);
    }
    return null;
  }
}
