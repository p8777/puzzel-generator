<?php 

include('C:/Program Files/adodb5/adodb.inc.php');

$driver = 'mysqli';
$server = 'localhost';
$user = 'root';
$password = '1234';
$database = 'website';

$db = adoNewConnection($driver);
$db->debug = true;
$db->connect($server, $user, $password, $database);

function getUserID($userID){
    global $db;
    $query = 'SELECT id FROM users WHERE id=?';
    $voorbereid = $db -> Prepare($query);
    $id = $db->execute($voorbereid,$userID);
    return $id->fields[0];
}
function addPuzzel(){
    global $db;
    $rows = 2;
    $col = 3;
    $save = 0;
    $hint = 1;
    $title = 'the cat';
    $img = 'hello.png';
    $reward = 'bad job';
    $query = "INSERT INTO puzzels (nRows,nColoms,pSave,hint,title,img,reward) 
            values (?,?,?,?,?,?,?)";
    $voorbereid = $db -> Prepare($query);
    $db -> Execute($voorbereid,array($rows,$col,$save,$hint,$title,$img,$reward));
}
function deletePuzzel($pid){
    global $db;
    $query = "DELETE FROM puzzels where Pid =?";
    $voorbereid = $db -> Prepare($query);
    $db -> Execute($voorbereid,$pid);
}
function addUserPuzzel($id,$pid){
    global $db;
    $query = "INSERT INTO users_puzzels (id,Pid) values (?,?)";
    $voorbereid = $db -> Prepare($query);
    $db -> Execute($voorbereid,array($id,$pid));
}
function updateUserPuzzel($id,$pid,$puzzel){
    global $db;
    $query = "UPDATE users_puzzels SET puzzelState = ? WHERE id=? AND Pid=?";
    $voorbereid = $db -> Prepare($query);
    $db -> Execute($voorbereid,array($puzzel,$id,$pid));
}
function deleteUserPuzzel($id,$pid){
    global $db;
    $query = "DELETE FROM users_puzzels WHERE id=? AND Pid=?";
    $voorbereid = $db -> Prepare($query);
    $db -> Execute($voorbereid,array($id,$pid));
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/applicatie.js"></script>
    <title>Puzzel generator</title>
</head>
<body>
    <h1>Hello World</h1>
    <h2><?php echo updateUserPuzzel(12345,4,'{"name":"John", "age":30, "car":null}') ?> </h2>
</body>
</html>
